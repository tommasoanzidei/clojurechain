(ns clojurechain.core
  (:gen-class))

(def fib-seq-seq
  ((fn fib [a b] 
      (lazy-seq (cons a (fib b (+ a b)))))
    0 1)) 

(def fib-seq-iterate
  (map first (iterate 
                (fn [[a b]] [b (+ a b)]) [0 1])))    

(defn -main
  "Fibonacci"
  [& args]
  (dorun
    (map println (take 40 fib-seq-seq))
  )
)
